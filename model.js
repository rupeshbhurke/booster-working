var exports = module.exports = {};

exports.Model = function (models, db) {
//   const readModel = function(filePath, encoding = 'utf8') {
//     const fs = require('fs');
//
//     var wrapperObject = JSON.parse(fs.readFileSync(filePath, encoding));
//     var modelObject = wrapperObject['model'];
//
//     models.forEach(model => {
//       const modelName = model['name'];
//       const fields = model['fields'];
//       //fields.forEach(field => {
//       //  const fieldName = field['name'];
//       //  const fieldType = field['type'];
//       //})
//       models[modelName] = db.define(modelName, fields);
//
//       //TODO: Pending ....
//       //models['comment'].hasOne('post', models['post'], { required: true, field: 'post', accessor: 'Post' });
//     });
//
// }

  models['post'] = db.define('post', {
    'title': String,
    'content': String,
    'other': String
  });

  models['comment'] = db.define('comment', {
    'comment': String
  });

  models['comment'].hasOne('post', models['post'], { required: true, field: 'post', accessor: 'Post' });

  models['person'] = db.define('person', {
    'FirstName': String,
    'LastName': String,
    'BirthDate': Date,
    'Remarks': String
  });

  return models;
};