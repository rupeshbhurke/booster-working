/*jslint node:true, debug:true, nomen:true */
const express = require('express'),
	_ = require('lodash'),
	booster = require('booster'),
	async = require('async'),
	PORT = 31057
const app = express();
const bodyParser = require('body-parser');
const resource = require('./resources');

//var isOrm = process.argv && process.argv.length > 2 && process.argv[2] === "orm2";
//app.use(express.bodyParser());
app.use(bodyParser.json());

//if (isOrm) {
const db = require('./db-orm2');
//} else {
//	db = require('./db');
//}

booster.init({ db: db, app: app, models: __dirname + '/resources/models' });
resource.setResources(booster);
app.listen(PORT);

console.log("send your requests to http://localhost :" + PORT);
