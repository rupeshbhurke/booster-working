var exports = module.exports={};

exports.setResources = function(booster)
{
    booster.resource('post');
    booster.resource('person');
    booster.resource('comment', { parent: 'post' });
    return booster;
};