var exports = module.exports={};

exports.Models = function(models, db)
{
    models.post = db.define("post", {
		title: String,
		content: String,
		other: String
    });

    models.comment = db.define("comment", {
		comment: String
    });
    
    models.comment.hasOne( "post", models.post, { required: true, field: "post", accessor: "Post" });
    
    models.person = db.define("person", {
      FirstName: String,
      LastName: String,
      BirthDate: Date,
      Remarks: String
    });
    
    return models;
};