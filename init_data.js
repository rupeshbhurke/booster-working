var exports = module.exports = {};

exports.Data = function () {
	var array = {};
	array['post'] = [
		{ title: "foo", content: "Lots of it" },
		{ title: "foobar", content: "Even more" },
		{ title: "bar", content: "phd" },
		{ title: "bar", content: "phd", other: "other field" }
	];
	array['comment'] = [
		{ post: 1, comment: "First comment on 1st post" },
		{ post: 1, comment: "Second comment on 1st post" },
		{ post: 3, comment: "First comment on 3rd post"  }
	];
	array['person'] = [
		{
			FirstName: "Rupesh", LastName: "Bhurke", BirthDate: "1977/03/19",
			Remarks: "Rupesh Hemchanda Bhurke"
		},
		{
			FirstName: "Usha", LastName: "Bhurke", BirthDate: "1976/04/11",
			Remarks: "Usha Rupesh Bhurke"
		},
		{
			FirstName: "Dev", LastName: "Bhurke", BirthDate: "2006/09/18",
			Remarks: "Dev Rupesh Bhurke"
		}
	];
	return array;
};